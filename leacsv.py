#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import string
import csv



# stackoverflow.com/a/12640614/1418165
def col2num (col):
    num = 0
    for c in col:
        num = num * 26 + (ord(c) - ord('a')) + 1
    return num -1

def to_offset (cols):
    return [col2num(col) for col in cols.lower().split(',')]

def reorg (row, src, dst, dst_max, default_fill):
    new_row = [default_fill] * dst_max
    for i in range(len(src)):
        new_row[dst[i]] = row[src[i]]
    return new_row


def concatenate_csvs (Data, Bdd, Src0, Dst0, Filling):
    Src = to_offset(Src0)
    Dst = to_offset(Dst0)
    if len(Src) != len(Dst):
        print("You lied: len({src}) != len({dst})".format(src=Src, dst=Dst))
        exit(2)

    SEP = ';'
    ENCODING = 'latin-1'

    Bdd_col_count = 0
    with open(Bdd, 'r', encoding=ENCODING) as f:
        liseur = csv.reader(f, delimiter=SEP)
        first_row = liseur.__next__()
        Bdd_col_count = len(first_row)
    if Bdd_col_count == 0:
        print("bdd.csv a zéro colonnes?")
        exit(3)
    print("Bdd_col_count = {count}".format(count=Bdd_col_count))

    with open(Data, 'r', encoding=ENCODING) as inputF:
        reader = csv.reader(inputF, delimiter=SEP)
        # rows = map(tuple, reader)
        with open(Bdd, 'a', encoding=ENCODING) as outputF:
            writer = csv.writer(outputF, delimiter=SEP)
            for row in reader:
                writer.writerow(reorg(row, Src, Dst, Bdd_col_count, Filling))
