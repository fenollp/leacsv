#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import sys

import leacsv

def main ():
    if len(sys.argv) != 5 + 1:
        print('Usage:\n\t./main.py ‹default fill›  ‹data.csv› ‹(data) a,b,c›  ‹bdd.csv› ‹(bdd) i,j,k›')
        exit(1)
    [fill,  data, src,  bdd, dst] = tuple(sys.argv[1:])
    leacsv.concatenate_csvs(data, bdd, src, dst, fill)

if __name__ == '__main__':
    main()
