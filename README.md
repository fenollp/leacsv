# leacsv • [Bitbucket](https://bitbucket.org/fenollp/leacsv)

Merge 2 CSV files given columns to associate.
Supports a default fill value.

Pour Léa <3


Usage exemple:

    ./main.py '#N/A' data/data.csv K,A,A,R,Q,B,S,T,V,W,L,c,i,j   data/bdd.csv D,F,G,S,T,Z,AE,AF,AN,AO,AV,a,c,b

